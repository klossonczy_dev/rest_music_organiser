# REST Music Organizer

## Getting Started
### Requirements 
>Windows 10 or one of the following Linux distribution: 
- CentOS, Debian, Fedora, Raspbian, or Ubuntu

>[Docker](https://www.docker.com/)

### How to deploy in Windows
Download and Install Docker for Windows: 
- [Docker Desktop Installer](https://desktop.docker.com/win/stable/Docker%20Desktop%20Installer.exe)

>In case docker promps to update WSL 2 Linux kernel:

- Download and Update WSL 2 Linux kernel for Windows: [link](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi)

>Open PowerShell with administrator privileges and run the following command to enable srcipt execution: `Set-ExecutionPolicy Unrestricted`

>Run deploy_windows.ps1 script to start the containers

>Visit [localhost:8888](localhost:8888) to start 

### How to deploy in Linux
>Visit docker website to follow the distro specific insturcions: 
- [click on the distro you want to use](https://docs.docker.com/engine/install/#server)

>Install docker

>Run deploy_linux.bash script to start containers

>Visit [localhost:8888](localhost:8888) to start 

## Third Party APIs

### Spotify API

- [Web API Documentation](https://developer.spotify.com/documentation/web-api/)
- [WeB API Node](https://github.com/thelinmichael/spotify-web-api-node)
- [Web API Node Authentication Example](https://github.com/spotify/web-api-auth-examples)

### Musixmatch API

- [Web API Documentation](https://developer.musixmatch.com/documentation)
- [Swagger SDK](https://github.com/musixmatch/musixmatch-sdk)

## Design

- [Material Design for Bootstrap](https://mdbootstrap.com/)