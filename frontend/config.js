
var publicAddress;
var port = 8888;


function setPublicAddress(address) {
    publicAddress = address;
}

function getPublicAddress() {
    return publicAddress;
}

function getPort() {
    return port;
}

module.exports = {
    setPublicAddress: setPublicAddress,
    getPublicAddress: getPublicAddress,
    getPort: getPort
};