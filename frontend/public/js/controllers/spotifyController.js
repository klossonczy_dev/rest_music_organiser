var backendPort = 8282;

angular.module('musicOrganizer', ['ngRoute'])
    .controller('spotify', ['$rootScope', '$scope', '$location', '$window', 'spotifyFactory',
        ($rootScope, $scope, $location, $window, spotifyFactory) => {
            $rootScope.backendAddress = `http://${$location.host()}:${backendPort}`;
            $scope.musicService = "Spotify";
            $scope.loginUrl = `${$rootScope.backendAddress}/login`;
            $scope.changeUserUrl = `${$rootScope.backendAddress}/logout`;
            $rootScope.frontPageUrl = `http://${$location.host()}:8888/`;

            $scope.playlistTracksUrl = `${$rootScope.frontPageUrl}views/Tracks.html`;

            $scope.getUser = () => {
                spotifyFactory.getUserInformation()
                    .then((body) => {
                        $scope.username = body.data.display_name;
                        $scope.userSpotifyLink = body.data.external_urls.spotify;
                        if(body.data.images!=""){                            
                            $scope.userSpotifyAvatarImg = body.data.images[0].url;
                        } else {
                            $scope.userSpotifyAvatarImg = '../../assets/Spotify-icon.png';
                        }                            
                        $scope.loginChangeUserLabel = "Change User";
                        $scope.user = body.data.id;

                        spotifyFactory.getUserPlaylists($scope.user)
                            .then((body) => {
                                $scope.userPlaylists = body.data.items;
                            }).catch((error) => {
                                console.error(`getPlaylists error: ${JSON.stringify(error, null, 4)}`);
                            });

                    }).catch((error) => {
                        console.error(`getUser error: ${JSON.stringify(error, null, 4)}`);
                        $scope.loginChangeUserLabel = "Log In";
                        $window.alert("Authentication Error! Please log in again!");
                    });
            };

            $scope.getPlaylistTracks = () => {
                var absoluteUrl = $location.absUrl();
                var playlistId = absoluteUrl.substring(absoluteUrl.indexOf('playlistId=') + 11)
                spotifyFactory.getPlaylistTracks(playlistId)
                    .then((body) => {
                        $scope.playlistTracks = body.data.items;
                    }).catch((error) => {
                        console.error(`getPlaylistTracks error: ${JSON.stringify(error, null, 4)}`);
                    });
            }
        }]);