angular.module('musicOrganizer')
  .controller('musixmatch', ['$scope', 'musixmatchFactory',
    ($scope, musixmatchFactory) => {
      $scope.getTrackLyrics = (track, artist) => {
        $scope.songName = track;
        musixmatchFactory.getTrackLyrics(track, artist)
          .then((body) => {
            $scope.lyrics = body.data;
          }).catch((error) => {
            console.error(`getLyrics error: ${JSON.stringify(error, null, 4)}`);
          });
      }
    }]);