angular.module('musicOrganizer')
    .factory('spotifyFactory', ['$rootScope', '$http', ($rootScope, $http) => {
        var factory = {};

        factory.getUserInformation = () => {
            return $http.get(`${$rootScope.backendAddress}/getUser`)
            .then((body) => {
                console.log(`getUserInformation successful!`);
                return body;
            }).catch((error) => {
                console.error(`getUserInformation error!`);
                return error;
            });
        }

        factory.getUserPlaylists = (userId) => {
            var config = {
                params: {
                    user_id: userId
                }
            };
            return $http.get(`${$rootScope.backendAddress}/getPlaylists`, config)
                .then((body) => {
                    console.log(`getUserPlaylists successful!`);
                    return body;
                }).catch((error) => {
                    console.error(`getUserPlaylists error!`);
                    return error;
                });
        }

        factory.getPlaylistTracks = (playlistId) => {
            var config = {
                params: {
                    playlist_id: playlistId
                }
            };
            return $http.get(`${$rootScope.backendAddress}/getPlaylistTracks`, config)
                .then((body) => {
                    console.log(`getPlaylistTracks successful!`);
                    return body;
                }).catch((error) => {
                    console.error(`getPlaylistTracks error!`);
                    return error;
                });
        }

        return factory;
    }]);