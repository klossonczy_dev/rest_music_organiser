angular.module('musicOrganizer')
    .factory('musixmatchFactory', ['$rootScope', '$http', ($rootScope, $http) => {
        var factory = {};

        factory.getTrackLyrics = (track, artist) => {
            var config = {
                params: {
                    artist: artist,
                    track: track
                }
            };
            return $http.get(`${$rootScope.backendAddress}/getLyrics`, config)
                .then((body) => {
                    console.log(`getTrackLyrics successful!`);
                    return body;
                }).catch((error) => {
                    console.error(`getTrackLyrics error: ${JSON.stringify(error, null, 4)}`);
                    return error;
                });
        }
        return factory;
    }]);