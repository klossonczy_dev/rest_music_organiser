var gulp = require('gulp');
var colors = require('colors');
var nodemon = require('gulp-nodemon');

var jsFiles = ['*.js', 'public/js/**/*.js'];

gulp.task('serve', () => {
    var nodemonOptions = {
        script: './app.js',
        delayTime: 1,
        watch: jsFiles
    };

    return nodemon(nodemonOptions)
        .on('restart',(event) => {
            console.log('MusicOrganizerFrontend Restarting...'.bold.green);
        });
});
