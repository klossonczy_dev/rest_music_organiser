
var express = require('express');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var morgan = require('morgan');
var ip = require('ip');
var config = require('./config');

var app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(methodOverride('X-HTTP-Method-Override'));
//app.use(morgan('combined'));

app.use(express.static(__dirname + '/public'));
app.all('*', (req, res) => {
    res.sendStatus(418);
    console.log(`\nHeader:${JSON.stringify(req.headers, null, 4)}`);
    console.log(`Body: ${JSON.stringify(req.body, null, 4)}`);
});

var server = app.listen(config.getPort(), () => {
    config.setPublicAddress(ip.address());
    console.log(`Server is listening on ${ip.address()}:${server.address().port}`);
});