#!/usr/bin/env node

var app = require('./app');
var ip = require('ip');
var config = require('./node_app/config/config');
var spotify = require('./node_app/modules/spotify');
var musixmatch = require('./node_app/modules/musixmatch/musixmatch');
var color = require('color');
var Logger = new (require('./node_app/modules/logger')).creatInstance(config.getPath(module.filename), config.getDebug());

if (process.argv[2] == '--debug') {
    Logger.log('Debug Mode'.bold.magenta);
    config.setDebug(true);
}

app.set('port', config.port);

var server = app.listen(app.get('port'), function () {
    Logger.info(`Server is listening on ${ip.address()}:${server.address().port}`);
    config.setListeningAddress(ip.address(), server.address().port);
    spotify.Spotify();
});