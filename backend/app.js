
var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var morgan = require('morgan');
var methodOverride = require('method-override');
//var routes = require('./node_app/routes/api');
var callback = require('./node_app/routes/callback');
var getLyrics = require('./node_app/routes/getLyrics');
var getPlaylist = require('./node_app/routes/getPlaylist');
var getPlaylistTracks = require('./node_app/routes/getPlaylistTracks');
var getUser = require('./node_app/routes/getUser');
var login = require('./node_app/routes/login');
var logout = require('./node_app/routes/logout');
var config = require('./node_app/config/config');
var Logger = new (require('./node_app/modules/logger')).creatInstance(config.getPath(module.filename), config.getDebug());

var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(methodOverride('X-HTTP-Method-Override'));
// if (config.getDebug()) {
//     app.use(morgan('combined'));
// }
//app.use('/', routes);
app.use('/', callback);
app.use('/', getLyrics);
app.use('/', getPlaylist);
app.use('/', getPlaylistTracks);
app.use('/', getUser);
app.use('/', login);
app.use('/', logout);
app.all('*', (req, res) => {
    res.sendStatus(418);
    //Logger.debug('\nHeader: ' + JSON.stringify(req.headers, null, 4));
    //Logger.debug('Body: ' + JSON.stringify(req.body, null, 4));
});

module.exports = app;