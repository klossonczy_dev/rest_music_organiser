var gulp = require('gulp');
var colors = require('colors');
var nodemon = require('gulp-nodemon');

var jsFiles = ['*.js', 'node_app/**/*.js'];

gulp.task('serve', () => {
    var nodemonOptions = {
        script: './MusicOrganizer.js',
        delayTime: 1,
        watch: jsFiles
    };

    return nodemon(nodemonOptions)
        .on('restart', (event) => {
            console.log('MusicOrganizer Restarting...'.bold.green);
        });
});
