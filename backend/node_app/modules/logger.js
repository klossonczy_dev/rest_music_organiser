
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;
const myFormat = printf(info => {
  return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
});


function creatInstance(path, isDebug) {
  if (isDebug) {
    var Instance = createLogger({
      level: 'info',
      format: combine(
        label({ label: path }),
        timestamp(),
        myFormat
      ),
      transports: [
        new transports.Console({
          level: 'debug',
          colorize: true,
          timestamp: false,
          prettyPrint: true
        }),
        new transports.File({
          filename: 'debug.log',
          level: 'debug'
        })
      ]
    })
  } else {
    var Instance = createLogger({
      level: 'info',
      format: combine(
        label({ label: path }),
        timestamp(),
        myFormat
      ),
      transports: [
        new transports.Console({
          level: 'info',
          colorize: true,
          timestamp: false,
          prettyPrint: true
        }),
        new transports.File({
          filename: 'info.log',
          level: 'info'
        })
      ]
    })
  }

  return Instance;
}

module.exports = {
  creatInstance: creatInstance
};