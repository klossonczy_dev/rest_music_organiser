
var config = require('../../config/config');

var Promise = require('promise');
var MusixmatchApi = require('./javascript-client/src/index');
var Logger = new (require('../logger')).creatInstance(config.getPath(module.filename), config.getDebug());

MusixmatchApi.ApiClient.instance.authentications['key'].apiKey = config.musixmatchApiKey;
var LyricsApi = new MusixmatchApi.LyricsApi();

function matcherLyricsGetGet(artist, track) {
    Logger.info(`matcherLyricsGetGet(${artist}, ${track}) called`)
    return new Promise((fulfill, reject) => {
        var opts = {
            format: "json", // {String} output format: json, jsonp, xml.
            qArtist: artist, // {String}
            qTrack: track // {String}
        };

        LyricsApi.matcherLyricsGetGet(opts, (error, data, response) => {
            Logger.debug(`error: ${error} ${data}: ${JSON.stringify(data, null, 4)} response: ${JSON.stringify(response, null, 4)}`);
            if (error) {
                Logger.error(`matcherLyricsGetGet error: ${error}`);
                reject(error);
            } else if (JSON.parse(response.text).message.header.status_code == 200) {
                var lyrics = JSON.parse(response.text).message.body.lyrics.lyrics_body;
                Logger.info(`matcherLyricsGetGet lyric: \n' ${lyrics}`);
                fulfill(lyrics);
            } else {
                var statusCode = JSON.parse(response.text).message.header.status_code;
                Logger.error(`matcherLyricsGetGet bad response. status code: ${statusCode}`);
                reject(statusCode);
            }
        });
    });
}

module.exports = {
    matcherLyricsGetGet: matcherLyricsGetGet
};