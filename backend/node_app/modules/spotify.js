
var spotify = require('spotify-web-api-node');
var config = require('../config/config');
var Promise = require('promise');
var request = require('request');
var Logger = new (require('./logger')).creatInstance(config.getPath(module.filename), config.getDebug());

var spotifyApi = new spotify({
    clientId: config.spotifyClientID,
    clientSecret: config.spotifyClientSecret,
    redirectUri: config.getSpotifyRedirectUri()
});

function Spotify() { }

function requestScopelessAccessToken() {
    Logger.info('requestScopelessAccessToken called');
    return new Promise((fulfill, reject) => {
        spotifyApi.clientCredentialsGrant().then(
            (data) => {
                Logger.info(`The access token expires in ${data.body['expires_in']}`);
                Logger.info(`The access token is ${data.body['access_token']}`);
                spotifyApi.setAccessToken(data.body['access_token']);
                fulfill(data.body['access_token']);
            },
            (err) => {
                Logger.error(`Something went wrong when retrieving an access token: ${err.message}`);
                reject(err.message)
            }
        );
    })
}

function setAccessToken() {
    Logger.info('setAccessToken called');
    return new Promise((fulfill, reject) => {
        var token = config.getSpotifyAccessToken();
        if (token == null) {
            var error = `Spotify Access Token is null(token:${token})`;
            Logger.error(`setAccessToken error: ${error}`);
            reject(error);
        } else {
            spotifyApi.setAccessToken(token);
            fulfill(token);
        }
    })
}

function authenticate(code) {
    Logger.info('authenticate called');
    return new Promise((fulfill, reject) => {
        var authOptions = {
            url: 'https://accounts.spotify.com/api/token',
            form: {
                code: code,
                redirect_uri: config.getSpotifyRedirectUri(),
                grant_type: 'authorization_code'
            },
            headers: {
                'Authorization': `Basic ${(new Buffer(`${config.spotifyClientID}:${config.spotifyClientSecret}`).toString('base64'))}`
            },
            json: true
        };

        request.post(authOptions,(error, response, body) => {
            if (!error && response.statusCode === 200) {
                fulfill(body);
            } else {
                reject(error);
            }
        });
    });

}

function getUserCreatedPlaylists(userId) {
    Logger.info(`getUserCreatedPlaylists(${userId}) called`);
    return new Promise((fulfill, reject) => {
        requestScopelessAccessToken().then((token) => {
            spotifyApi.getUserPlaylists(userId).then((res) => {
                Logger.info(`User's Playlists (${res.body.total}): `);
                res.body.items.forEach(element => {
                    Logger.info(`${element.name} id: ${element.id}`);
                });
                fulfill(res);
            }).catch((error) => {
                Logger.error(`getUserCreatedPlaylists Error: ${error}`);
                reject(error);
            });
        }).catch((error) => {
            Logger.error(`Search error: requestScopelessAccessToken error: ${error}`);
            reject(error);
        });
    })
}

function getPlaylistTracks(playlistId) {
    Logger.info('getPlaylistTracks called');
    return new Promise((fulfill, reject) => {
        requestScopelessAccessToken().then((token) => {
            spotifyApi.getPlaylistTracks(playlistId).then((res) => {
                Logger.info(`Playlist's Tracks (${res.body.total}): `);
                res.body.items.forEach(element => {
                    Logger.info(`${element.track.name} id: ${element.track.id}`);
                });
                fulfill(res);
            }).catch((error) => {
                Logger.error(`getPlaylistTracks Error: ${error}`);
                reject(error);
            });
        }).catch((error) => {
            Logger.error(`Search error: requestScopelessAccessToken error: ${error}`);
            reject(error);
        });
    })
}

function getUser() {
    Logger.info('getUser called');
    return new Promise((fulfill, reject) => {
        setAccessToken().then((token) => {
            spotifyApi.getMe().then((res) => {
                Logger.debug(`getUser: ${JSON.stringify(res, null, 4)}`);
                fulfill(res);
            }).catch((error) => {
                Logger.error(`getUser Error: ${error}`);
                reject(error);
            });
        }).catch((error) => {
            Logger.error(`Search error: setAccessToken error: ${error}`);
            reject(error);
        });
    })
}

//Functions for further developement

function search(query, type) {
    Logger.info(`Search(${query}, ${type}) function called`);
    return new Promise((fulfill, reject) => {
        requestScopelessAccessToken().then((token) => {
            spotifyApi.search(query, type, { 'token': token }).then((data) => {
                var number = data.body.tracks.total;
                Logger.info(`Search(${query}, ${type}) function got total of ${number} results!`);
                if (number >= config.spotifyMaxResultNumber) {
                    Logger.warn(`Search(${query}, ${type}) request has yielded too many results (${config.spotifyMaxResultNumber}/${number}) to display! `);
                    fulfill(results);
                }
                var results = data.body.tracks.items;
                results.forEach((track, index) => {
                    Logger.info(`${index}: ${track.name} (${track.popularity}) album: ${track.album.name} artist: ${track.album.artists[0].name}`);
                });
                fulfill(results);
            }).catch((error) => {
                Logger.error(`Search(${query}, ${type}) function error: ${error.message}`);
                reject(error);
            });
        }).catch((error) => {
            Logger.error(`Search error: requestScopelessAccessToken error: ${error}`);
            reject(error);
        });
    })
}

function getPlaylist(playlistId) {
    Logger.info('getPlaylist called');
    return new Promise((fulfill, reject) => {
        requestScopelessAccessToken().then((token) => {
            spotifyApi.getPlaylist(playlistId).then((res) => {
                Logger.info(`getPlaylist: ${JSON.stringify(res.body)}`);
                fulfill(res.body);
            }).catch((error) => {
                Logger.error(`getPlaylist Error: ${error}`);
                reject(error);
            });
        }).catch((error) => {
            Logger.error(`Search error: requestScopelessAccessToken error: ${error}`);
            reject(error);
        });
    })
}

function createPlaylist(userid, name, isPublic) {
    Logger.info('createPlaylist called');
    return new Promise((fulfill, reject) => {
        requestScopelessAccessToken().then((token) => {
            spotifyApi.createPlaylist(userid, name, { 'public': isPublic }).then((res) => {
                Logger.info('Playlist Created');
                fulfill(res);
            }).catch((error) => {
                Logger.error(`createPlaylist Error: ${error}`);
                reject(error);
            });
        }).catch((error) => {
            Logger.error(`Search error: requestScopelessAccessToken error: ${error}`);
            reject(error);
        });
    })
}

function addTracksToPlaylist(playlistId, tracks) {
    Logger.info('addTrackToPlaylist called');
    return new Promise((fulfill, reject) => {
        requestScopelessAccessToken().then((token) => {
            spotifyApi.addTracksToPlaylist(playlistId, tracks).then((res) => {
                Logger.info('Added Tracks to Playlist');
                fulfill(res);
            }).catch((error) => {
                Logger.error(`addTrackToPlaylist Error: ${error}`);
                reject(error);
            });
        }).catch((error) => {
            Logger.error(`Search error: requestScopelessAccessToken error: ${error}`);
            reject(error);
        });
    })
}

module.exports = {
    Spotify: Spotify,
    getUser: getUser,
    getUserCreatedPlaylists: getUserCreatedPlaylists,
    getPlaylistTracks: getPlaylistTracks,
    authenticate: authenticate
};