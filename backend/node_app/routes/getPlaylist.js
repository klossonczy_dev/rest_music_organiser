
var config = require('../config/config');
var express = require('express');
var router = express.Router();
var spotify = require('../modules/spotify');
var querystring = require('querystring');

var Logger = new (require('../modules/logger')).creatInstance(config.getPath(module.filename), config.getDebug());

router.use((req, res, next) => {
    next();
});

router.route('/getPlaylists')
    .get((request, response) => {
        var userId = JSON.parse(JSON.stringify(request.query)).user_id;
        Logger.info(`[getPlaylists] Incoming GET request (${querystring.stringify(request.query)}) from ${request.rawHeaders[1]}`);
        spotify.getUserCreatedPlaylists(userId).then((data) => {
            Logger.info('[getPlaylists] sent ');
            Logger.debug(`[getPlaylists] sent: ${JSON.stringify(data.body)}`);
            response.setHeader('Content-Type', 'application/json');
            response.setHeader('Access-Control-Allow-Origin', '*');
            response.status(200).send(JSON.stringify(data.body));
        }).catch((error) => {
            Logger.error(`[getPlaylists] error: ${error}`);
            response.setHeader('Content-Type', 'application/json');
            response.setHeader('Access-Control-Allow-Origin', '*');
            response.status(400);
            response.send(JSON.stringify(error));
        });
    });

module.exports = router;
