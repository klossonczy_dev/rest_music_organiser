
var config = require('../config/config');
var express = require('express');
var router = express.Router();
var musixmatch = require('../modules/musixmatch/musixmatch');
var querystring = require('querystring');

var Logger = new (require('../modules/logger')).creatInstance(config.getPath(module.filename), config.getDebug());

router.use((req, res, next) => {
    next();
});

router.route('/getLyrics')
    .get((request, response) => {
        var artist = JSON.parse(JSON.stringify(request.query)).artist;
        var track = JSON.parse(JSON.stringify(request.query)).track;
        Logger.info(`[getLyrics] Incoming GET request (${querystring.stringify(request.query)}) from ${request.rawHeaders[1]}`);
        musixmatch.matcherLyricsGetGet(artist, track).then((lyrics) => {
            Logger.info('[getLyrics] sent ');
            Logger.debug(`[getLyrics] sent: ' ${JSON.stringify(lyrics)}`);
            response.setHeader('Content-Type', 'application/json');
            response.setHeader('Access-Control-Allow-Origin', '*');
            response.status(200).send(JSON.stringify(lyrics));
        }).catch((error) => {
            Logger.error(`[getLyrics] error: ${error}`);
            response.setHeader('Content-Type', 'application/json');
            response.setHeader('Access-Control-Allow-Origin', '*');
            response.status(400);
            response.send(JSON.stringify(error));
        });
    });

module.exports = router;
