
var config = require('../config/config');
var express = require('express');
var router = express.Router();
var querystring = require('querystring');

var Logger = new (require('../modules/logger')).creatInstance(config.getPath(module.filename), config.getDebug());

var generateRandomString = (length) => {
    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
};

router.use((req, res, next) => {
    next();
});

router.route('/login')
    .get((req, response) => {
        Logger.info(`[login] Incoming GET request from:
                     ${req.header('referer')}`);
        Logger.debug(`[login] rawHeaders: ${req.rawHeaders}`);

        var stateKey = 'spotify_auth_state';
        var refererAddress = 
            req.rawHeaders[1].substring(
                0, 
                req.rawHeaders[1].indexOf(':'));
        config.setSpotifyRedirectUri(
            refererAddress,
             config.port,
              config.spotifyCallbackEndbpointName)
        config.setSpotifyState(generateRandomString(16));


        response.cookie(stateKey, config.getSpotifyState());
        response.redirect(`https://accounts.spotify.com/authorize?${
            querystring.stringify({
                response_type: 'code',
                client_id: config.spotifyClientID,
                scope: config.getSpotifyScope(),
                redirect_uri: config.getSpotifyRedirectUri(),
                state: config.getSpotifyState()
            })}`);
    });

module.exports = router;
