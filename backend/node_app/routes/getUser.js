
var config = require('../config/config');
var express = require('express');
var router = express.Router();
var spotify = require('../modules/spotify');

var Logger = new (require('../modules/logger')).creatInstance(config.getPath(module.filename), config.getDebug());

router.use((req, res, next) => {
    next();
});

router.route('/getUser')
    .get((request, response) => {
        Logger.info(`[getUser] Incoming GET request from ${request.rawHeaders[1]}`);
        spotify.getUser().then((data) => {
            response.setHeader('Content-Type', 'application/json');
            response.setHeader('Access-Control-Allow-Origin', '*');
            response.status(200);
            response.send(JSON.stringify(data.body));
            Logger.info('[getUser] sent ');
            Logger.debug(`[getUser] sent: ${JSON.stringify(data.body)}`);
        }).catch((error) => {
            response.setHeader('Content-Type', 'application/json');
            response.setHeader('Access-Control-Allow-Origin', '*');
            response.status(400);
            response.send(JSON.stringify(error));
            Logger.error(`[getUser] error: ${error}`);
        });
    });


module.exports = router;
