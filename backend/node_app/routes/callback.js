
var config = require('../config/config');
var express = require('express');
var router = express.Router();
var spotify = require('../modules/spotify');
var querystring = require('querystring');

var Logger = new (require('../modules/logger')).creatInstance(config.getPath(module.filename), config.getDebug());

var stateKey = 'spotify_auth_state';

router.use((req, res, next) => {
    next();
});

router.route('/callback')
    .get((req, res) => {
        Logger.info(`[callback] Incoming GET request from ${req.header('referer')}`);
        Logger.debug(`[callback] req.rawHeaders=${req.rawHeaders[1].substring(0, req.rawHeaders[1].indexOf(':'))}`);
        Logger.debug(`[callback] query: ${JSON.stringify(req.query, null, 4)}`);
        Logger.debug(`[callback] headers: ${JSON.stringify(req.headers, null, 4)}`);

        var refererAddress = req.rawHeaders[1].substring(0, req.rawHeaders[1].indexOf(':'));
        var code = req.query.code || null;
        var state = req.query.state || null;
        var cookie = req.headers.cookie || null;
        var storedState = cookie ? cookie.substring(cookie.indexOf('=') + 1) : null;

        if (state === null || state !== storedState) {
            res.redirect(`http://${refererAddress}:8888/${querystring.stringify({error: 'state_mismatch'})}`);
        } else {
            res.clearCookie(stateKey);

            spotify.authenticate(code).then((body)=> {
                config.setSpotifyAccessToken(body.access_token);
                config.setSpotifyRefreshToken(body.refresh_token);

                //spotify.getUser();

                Logger.info(`Redirecting to: http://${refererAddress}:8888/views/Playlists.html`);
                res.redirect(`http://${refererAddress}:8888/views/Playlists.html`);
            }).catch(() => {
                res.redirect(`http://${refererAddress}:8888/#
                    ${querystring.stringify({
                        error:'invalid_token'
                    })}`);
            })
        }
    });

module.exports = router;
