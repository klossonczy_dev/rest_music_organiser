
var path = require('path');
var Logger = new (require('../modules/logger')).creatInstance(getPath(module.filename),debug);

var internalAddress = '127.0.0.1';
var publicAddress;
var port = 8282;
var debug = false;
var spotifyCallbackEndbpointName = 'callback';

var spotifyRedirectUri;
var spotifyAuthorizationUrl;
var spotifyAccessToken;
var spotifyRefreshToken;
var spotifyMaxResultNumber = 20;
var spotifyScope = ['user-read-private', 'user-read-email', 'playlist-read-private', 'user-read-birthdate'];
var spotifyState;

var musixmatchApiKey = "59a7bace3327c8e0f9fdc07a22a06a88";

function getPath(filename) {
    return JSON.stringify(filename)
        .substr(JSON.stringify(filename)
            .indexOf(path.dirname(filename).split(path.sep).pop()))
}

function setListeningAddress(ip,port) {
    Logger.info(`setListeningIp(${ip}, ${port} ) called`);
    publicAddress = ip;
    port = port;
}

function getPublicAddress() {
    return publicAddress;
}


function setSpotifyRedirectUri(ip,port,endpoint) {
    spotifyRedirectUri = `http://${ip}:${port}/${endpoint}`;
}

function getSpotifyRedirectUri() {
    return spotifyRedirectUri;
}

function setSpotifyAuthorizationUrl(url) {
    Logger.info(`setSpotifyAuthorizationUrl(${url}) called`);
    spotifyAuthorizationUrl = url;
}

function getSpotifyAuthorizationUrl() {
    return spotifyAuthorizationUrl;
}

function setDebug(debug) {
    debug = debug;
}

function getDebug(){
    return debug;
}

function setSpotifyAccessToken(token) {
    spotifyAccessToken = token;
}

function getSpotifyAccessToken(){
    return spotifyAccessToken;
}

function setSpotifyRefreshToken(token) {
    spotifyRefreshToken = token;
}

function getSpotifyRefreshToken() {
    return spotifyRefreshToken;
}

function getSpotifyScope() {
    return spotifyScope;
}

function setSpotifyScope(scope) {
    spotifyScope = scope;
}

function getSpotifyState() {
    return spotifyState;
}

function setSpotifyState(state) {
    spotifyState = state;
}

module.exports = {
    address: internalAddress,
    port: port,
    apiURL: internalAddress + '/api',
    fileHostBaseURL: internalAddress + '/static',
    spotifyClientID: '73b9d5823748479bb79ba22710be6ba2',
    spotifyClientSecret: 'ce588a24698b432e90036a230f066423',
    getSpotifyScope: getSpotifyScope,
    setSpotifyScope: setSpotifyScope,
    getSpotifyState: getSpotifyState,
    setSpotifyState: setSpotifyState,
    getPath: getPath,
    setListeningAddress: setListeningAddress,
    getPublicAddress: getPublicAddress,
    setSpotifyAuthorizationUrl: setSpotifyAuthorizationUrl,
    getSpotifyAuthorizationUrl: getSpotifyAuthorizationUrl,
    setSpotifyRedirectUri: setSpotifyRedirectUri,
    getSpotifyRedirectUri: getSpotifyRedirectUri,
    setDebug: setDebug,
    getDebug: getDebug,
    setSpotifyAccessToken: setSpotifyAccessToken,
    getSpotifyAccessToken: getSpotifyAccessToken,
    setSpotifyRefreshToken: setSpotifyRefreshToken,
    getSpotifyRefreshToken: getSpotifyRefreshToken,
    spotifyMaxResultNumber: spotifyMaxResultNumber,
    spotifyCallbackEndbpointName: spotifyCallbackEndbpointName,
    musixmatchApiKey: musixmatchApiKey
};