

Write-Host updating project...
git pull

Write-Host build backend image...
docker build -t mo_backend:1.0.0 backend/.

Write-Host build frontend image...
docker build -t mo_frontend:1.0.0 frontend/.

Write-Host removing old containers...
docker stop mobackend
docker stop mofrontend
docker rm mobackend
docker rm mofrontend

Write-Host starting new containers...
docker run --name=mobackend -d -p 8282:8282 mo_backend:1.0.0
docker run --name=mofrontend -d -p 8888:8888 mo_frontend:1.0.0

Read-Host -Prompt "Press Enter to exit"