#!/bin/bash

echo updating project...
sudo git pull

echo build backend image...
sudo docker build -t mo_backend:1.0.0 backend/.

echo build frontend image...
sudo docker build -t mo_frontend:1.0.0 frontend/.

echo removing old containers...
sudo docker stop mobackend
sudo docker stop mofrontend
sudo docker rm mobackend
sudo docker rm mofrontend

echo starting new containers...
sudo docker run --name=mobackend -d -p 8282:8282 mo_backend:1.0.0
sudo docker run --name=mofrontend -d -p 8888:8888 mo_frontend:1.0.0
